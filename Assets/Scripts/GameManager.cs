﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

    [Header("Enemy1")]
    [SerializeField] GameObject Food;
    [SerializeField] public float spawnTime = 3f;
    public float enemy1PerSpawn = 1f;
    [SerializeField] Tilemap tileMap;
    public List<Vector3> availablePlaces;





    int level;
    [SerializeField] Text levelText;
    ShipMovement shipScript;
    GameObject ship;
    // Start is called before the first frame update
    void Start()
    {
        ship = GameObject.Find("PlayerCompanion");
        shipScript = ship.GetComponent<ShipMovement>();
       
        


        //Position von jedem Floortile ermitteln:
        availablePlaces = new List<Vector3>();

        for (int n = tileMap.cellBounds.xMin; n < tileMap.cellBounds.xMax; n++)
        {
            for (int p = tileMap.cellBounds.yMin; p < tileMap.cellBounds.yMax; p++)
            {
                Vector3Int localPlace = (new Vector3Int(n, p, (int)tileMap.transform.position.y));
                Vector3 place = tileMap.CellToWorld(localPlace);
                if (tileMap.HasTile(localPlace))
                {
                    availablePlaces.Add(place);
                }
            }
        }


        if (Food != null)
        {
            StartCoroutine("SpawnEnemy1");
        }
    }

    // Update is called once per frame
    void Update()
    {
        levelText.text = shipScript.shipLevel.ToString();
        achievementReached();
    }



    [SerializeField] GameObject Dingy1;
    [SerializeField] GameObject Dingy2;
    [SerializeField] GameObject Dingy3;
    [SerializeField] GameObject Ship1;
    [SerializeField] GameObject Ship2;
    [SerializeField] GameObject Ship3;
    // [SerializeField] Sprite _shipSprite; try to learn to change ships sprites
    //change ship quality based on health
    void achievementReached()
    {
        if (shipScript.shipLevel == 10){ Dingy1.SetActive(true); }
        if (shipScript.shipLevel == 20) { Dingy2.SetActive(true); }
        if (shipScript.shipLevel == 30) { Dingy3.SetActive(true); }
        if (shipScript.shipLevel == 40) { Ship1.SetActive(true);
            Dingy1.SetActive(false);
        }
        if (shipScript.shipLevel == 50) { Ship2.SetActive(true);
            Dingy2.SetActive(false);
        }
        if (shipScript.shipLevel == 60) { Ship3.SetActive(true);
            Dingy3.SetActive(false);
        }

    }


    IEnumerator SpawnEnemy1()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnTime);

            for (int i = 0; i < enemy1PerSpawn; i++)
            {
                Instantiate(Food,
                     availablePlaces[Random.Range(0, availablePlaces.Count)],
                     Quaternion.identity);
                Debug.Log("enemy spawned");
            }

        }
    }


}
