﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipOutOfBounds : MonoBehaviour
{

    ShipMovement shipScript;
    GameObject ship;
    public float _originalSpeed;
    [SerializeField]public float punishSpeed;
    // Start is called before the first frame update
    void Start()
    {
        ship = GameObject.Find("PlayerCompanion");
        shipScript = ship.GetComponent<ShipMovement>();
        
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.gameObject.CompareTag("Ship"))
        {
            _originalSpeed = shipScript.ShipSpeed;
            shipScript.ShipSpeed = punishSpeed;
            StartCoroutine("PlayerHurt");
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Ship"))
        {
            shipScript.ShipSpeed = _originalSpeed;
            StopCoroutine("PlayerHurt");
        }
    }

    IEnumerator PlayerHurt()
    {
        yield return new WaitForSeconds(2f);
        //shipScript.ShipSpeed = punishSpeed; Damage player
    }
}
