﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class ShipMovement : MonoBehaviour
{

    RaycastHit2D hit;
    [SerializeField] GameObject Player;
    [SerializeField] float RotateSpeed = 0;
    [SerializeField] public float ShipSpeed = 0;
    [SerializeField] public bool moveableMouseLocation =  false;

    [SerializeField] public Tilemap water;

    [SerializeField] public static int Health;
    [SerializeField] public int shipLevel;
    Vector3 vectorToTarget;
    float angle;


    // Start is called before the first frame update
    void Start()
    {
        moveableMouseLocation = true;

        Health = 10;
        Physics2D.IgnoreCollision(water.GetComponent<Collider2D>(), GetComponent<Collider2D>());
    }

    // Update is called once per frame

    private void FixedUpdate()
    {

        if (moveableMouseLocation)
        {

            if (transform.position != Player.transform.position)
            {


                //Object Rotation
                Vector3 vectorToTarget = Player.transform.position - transform.position;
                angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + 90f;
                Quaternion qt = Quaternion.AngleAxis(angle, Vector3.forward);
                transform.rotation = Quaternion.RotateTowards(transform.rotation, qt, Time.deltaTime * (RotateSpeed * 10));

                //Check if rotation = constant
                if (transform.rotation == qt)
                {
                    //object movement
                    Vector3 targetDirection = Player.transform.position - transform.position;
                    transform.position = Vector3.MoveTowards(transform.position, Player.transform.position, ShipSpeed / 1000);

                }
            }
        }
    }
    void Update()
    {
        /*
        Health++;
        if (Health > 10)
        {
            Health -= 10;
        }
        */
    }

}
