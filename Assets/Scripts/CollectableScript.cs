﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour
{
    [SerializeField] int level;
    ShipMovement shipScript;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
 
    void OnTriggerEnter2D(Collider2D collision)
    {
        bool isEnemy = false; ;
                if(collision.gameObject.CompareTag("Ship") || collision.gameObject.CompareTag("Dingy"))
            {
                isEnemy = true;
            }
        if (isEnemy)
        {
                GameObject ship = GameObject.Find("PlayerCompanion");
            shipScript =  ship.GetComponent<ShipMovement>();
            shipScript.shipLevel++;
            
            Destroy(gameObject, 0.5f);
        }


        if (collision.gameObject.CompareTag("Food"))
        {
            Debug.Log("Food collides");
            Destroy(gameObject);
            Destroy(collision.gameObject);
        }
    }

}
