﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseMovement : MonoBehaviour
{
    public bool moveable;
    // Start is called before the first frame update
    void Start()
    {
        moveable = true;
    }
    // Update is called once per frame
    void Update()
    {
        //Mouse Location;
        Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        point.z = 1;
        Debug.Log("Mouse Movement " + moveable);
        if (moveable)
        {

            transform.position = point;
        }
    }
}
