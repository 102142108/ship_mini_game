﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] float speed;
    float inputX, inputY;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        inputX = Input.GetAxis("Horizontal");
        inputY = Input.GetAxis("Vertical");
        if (inputX != 0)
        {
            moveAcross();
        }
        if (inputY != 0)
        {
            moveAbove();
        }
    }

    private void moveAcross()
    {
        transform.position += transform.right * (inputX * speed) * Time.deltaTime;
    }
    private void moveAbove()
    {
        transform.position += transform.up * (inputY * speed)* Time.deltaTime ;
    }

}
