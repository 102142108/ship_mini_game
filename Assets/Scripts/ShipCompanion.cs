﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipCompanion : MonoBehaviour
{
    [SerializeField] GameObject Player;
    [SerializeField] GameObject PlayerCompanion;
    [SerializeField] float RotateSpeed;
    [SerializeField] float ShipSpeed;
    [SerializeField] public static int Health;
    Vector3 vectorToTarget;
    float angle;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        if (PlayerCompanion.transform.position != Player.transform.position)
        {


            //Object Rotation
            Vector3 vectorToTarget = Player.transform.position - PlayerCompanion.transform.position;
            angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg + 90f;
            Quaternion qt = Quaternion.AngleAxis(angle, Vector3.forward);
            PlayerCompanion.transform.rotation = Quaternion.RotateTowards(PlayerCompanion.transform.rotation, qt, Time.deltaTime * (RotateSpeed * 10));

            //Check if rotation = constant
            if (PlayerCompanion.transform.rotation == qt)
            {
                //object movement
                Vector3 targetDirection = Player.transform.position - PlayerCompanion.transform.position;
                PlayerCompanion.transform.position = Vector3.MoveTowards(PlayerCompanion.transform.position, Player.transform.position, ShipSpeed / 10000);
                //PlayerCompanion.transform.position = Vector3.MoveTowards(PlayerCompanion.transform.position, point, .0029f);
            }
        }

    }
}
