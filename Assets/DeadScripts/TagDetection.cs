﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TagDetection : MonoBehaviour
{
    bool moveable;
    // Start is called before the first frame update
    void Start()
    {
       
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Ground")
        {
            moveable = false;
            Debug.Log("CollisonTag Movement " + moveable);
        }
        else { moveable = true; }
    }
    // Update is called once per frame

    void OnMouseEnter() { Debug.Log("I am over something"); }
    void Update()
    {
        
        Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        point.z = 1;
            transform.position = point;
            if (moveable == false)
        {
            GameObject.Find("Player").GetComponent<MouseMovement>().moveable = false;

        }
        else
        {
            GameObject.Find("Player").GetComponent<MouseMovement>().moveable = true;
        }
        Debug.Log("Tag Movement " + moveable);
    }
}
